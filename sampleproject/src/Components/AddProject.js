import React, { Component } from 'react';


class AddProject extends Component {
    static defaultProps = {
        categories: ['web design', 'web development', 'mobile development']
    }

    render() {

        let categoryOption = this.props.categories.map(
            category => {
                return <option key={category} value="category">{category}</option>
            }
        )
        return (
            <div>
                <h3>Add Project</h3>
                <div>
                    <label>Title</label><br />
                    <input type="text" ref="title"/>
                </div>
                <div>
                    <label>Category</label><br />
                    <select ref="category">
                        {categoryOption}
                    </select>
                </div>
            </div>
        );
    }
}

export default AddProject;