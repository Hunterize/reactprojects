import React, { Component } from 'react';
import Projects from './Components/Projects';
import AddProject from './Components/AddProject';
import './App.css';

class App extends Component {
    constructor() {
        super();
        this.state = {
            Projects:[]
        }
    }

    componentWillMount() {
        this.setState({
            Projects: [
                {
                    title: 'business website',
                    category: 'web design'
                },
                {
                    title: 'social app',
                    category: 'mobile development'
                },
                {
                    title: 'ecommerce shopping cart',
                    category: 'web development'
                }
            ]
        });
    }


  render() {
    return (
      <div className="App">
            <AddProject />
            <br/>
            <Projects projects={this.state.Projects} />    
      </div>
    );
  }
}

export default App;
