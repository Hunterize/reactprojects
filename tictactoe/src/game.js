import React, { Component } from 'react';


//square function component
function Square(props){
    return (
        <button className="square"
            onClick={() => props.clickEvent()}>
            {props.data}
        </button>
    );
}

//board class component
class Board extends React.Component {

    renderSquare(i) {
        return (
            <Square
                data={this.props.cells[i]}
                clickEvent={() => this.props.clickEvent(i)}
            />
        );
    }

    render() {
        return (
            <div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
        );
    }


}

//game class component
class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //board list for game history
            history: [{
                cells: Array(9).fill(null),
            }],
            //current player
            isXTurn: true,
            //game history step
            step: 0,
        }
    }

    //winner check, parameter cells is the array of the board cells
    checkWin(cells) {
        const rules = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];

        for (let i = 0; i < rules.length; i++) {
            const [a, b, c] = rules[i];
            if (cells[a] && cells[a] === cells[b] && cells[c] === cells[a]) {
                return cells[a];
            }

        }

        return null;

    }

    //click event 
    handleClick(i) {
        //get the history list starting from the current step
        const history = this.state.history.slice(0, this.state.step + 1);
        const current = history[history.length - 1];
        const squares = current.cells.slice();
        if (!squares[i] && !this.checkWin(squares)) {
            squares[i] = this.state.isXTurn ? 'X' : 'O';
            //add board history to the end of the list
            this.setState({
                history: history.concat([{
                    cells: squares,
                    }]),
                isXTurn: this.state.isXTurn ? false : true,
                step: history.length,
            });

        }
    }

    jumpTo(index) {
        this.setState({
            step: index,
            isXTurn: (index % 2) == 0,
        });
    }

    render() {
        const history = this.state.history;
        const current = history[this.state.step];
        const winner = this.checkWin(current.cells);


        const moves = history.map((value, index) => {
            const desc = index ? 'Go to move #' + index : 'Go to game start';

            return (
                <li key={index}>
                    <button onClick={() => this.jumpTo(index)}>{desc}</button>
                </li>
                );
        });


        let status;

        if (winner) {
            status = 'Winner: ' + winner;
        } else {
            status = 'Next Player: ' + (this.state.isXTurn ? 'X' : 'O');
        }

        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        cells={current.cells}
                        clickEvent={(i) => this.handleClick(i)}
                    />
                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }
}

export default Game;